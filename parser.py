import csv
import ldap
import ldap.modlist as modlist
import random
import string
import smtplib
import mysql.connector as mysql
from passlib.context import CryptContext

# CONTEXTO
# Algoritmo para encriptacion de passwords
pwd_context = CryptContext(
        schemes=["pbkdf2_sha256"],
        default="pbkdf2_sha256",
        pbkdf2_sha256__default_rounds=30000
)

# Conexion a db MySQL
db = mysql.connect(
        host = '127.0.0.1',
        user = 'admin',
        passwd = 'admin',
        database = 'meli'
    )

print(db)
cursor = db.cursor(buffered = True)

# Metodo de insercion de datos de usuario en MySQL
add_user = ("INSERT INTO usuarios_ldap "
            "(nombre, apellido, email, password) "
            "VALUES (%s, %s, %s, %s)")

update_user = ("UPDATE usuarios_ldap "
               "SET password = %s "
               "WHERE email = %s")


# Conexion contra casilla saliente de Gmail
try:
    server = smtplib.SMTP('smtp.gmail.com:587')
    server.starttls()
    server.login('challengermeli@gmail.com', 'E4Qda3thiH8tqez')
#    server.sendmail(sent_from, to, body)
#    server.quit()

#    print('Email sent!')

except:
    print('Something went wrong with email...')

# Conexion contra LDAP
con = ldap.initialize('ldap://127.0.0.1:389', bytes_mode=False)
#con.simple_bind_s(u'cn=admin,dc=example,dc=com', u'admin')

# MENU
print("BUENOS DIAS\n")
print("Ingrese la opcion requerida:\n")
print("1) Insertar datos desde directorio.csv y enviar emails\n")
print("2) Acceder con su email y password\n")
print("3) Consultas bases de datos\n")
print("4) Limpiar bases de datos\n")

opcion = int(input("-> "))

if opcion == 1:
    reader = csv.DictReader(open("directorio.csv"))
    for raw in reader:

        print(raw["nombre"])
        print(raw["apellido"])
        print(raw["email"])

        def randomString(stringLength=10):
                """Generate a random string of fixed length """
                letters = string.ascii_lowercase
                return ''.join(random.choice(letters) for i in range(stringLength))

        clave = randomString()
        print(clave)

        encripted = pwd_context.encrypt(clave)
        print(encripted)

        user_data = (raw["nombre"], raw["apellido"], raw["email"], encripted)
        cursor.execute(add_user, user_data)
        db.commit()

        sent_from = 'challengermeli@gmail.com'
        to = [raw["email"]]
        subject = 'OMG Super AWESOME Message'
        body = raw["nombre"] + " " + raw["apellido"] + " " + raw["email"] + " " + clave

        server.sendmail(sent_from, to, body)
        print('email sent')

        dn = "uid=" + raw["email"] + ",ou=People,dc=example,dc=com"
        modlist = {
            "objectClass": [b"inetOrgPerson", b"posixAccount", b"shadowAccount"],
            "uid": [raw["email"].encode()],
            "sn": [raw["apellido"].encode()],
            "givenName": [raw["nombre"].encode()],
            "cn": [raw["nombre"].encode() + b" " + raw["apellido"].encode()],
            "displayName": [raw["nombre"].encode() + b" " + raw["apellido"].encode()],
            "mail": [raw["email"].encode()],
            "userPassword": [clave.encode()],
            "uidNumber": [b"5000"],
            "gidNumber": [b"10000"],
            "loginShell": [b"/bin/bash"],
            "homeDirectory": [b"/home/" + raw["nombre"].encode()]
            }

        con.simple_bind_s(u'cn=admin,dc=example,dc=com', u'admin')        
        result = con.add_s(dn, ldap.modlist.addModlist(modlist))

elif opcion == 2:
    uid = input("Ingrese email: ")
    print(uid)
    clave = input("\nIngrese clave: ")
    print(clave)

    dn = 'uid=' + uid + ',ou=People,dc=example,dc=com'
    print(dn)

    con.simple_bind_s(dn, clave.encode())

    new_pass = input('\nIngrese un nuevo password: ')

    old = {'userPassword': [clave.encode()]}
    new = {'userPassword': [new_pass.encode()]}

    ldif = modlist.modifyModlist(old, new)
    con.modify_s(dn, ldif)

    encripted = pwd_context.encrypt(clave)
    user_data = (encripted, uid)
    cursor.execute(update_user, user_data)
    db.commit()

    print(encripted)
    print("PASSWORD ACTUALIZADO")


elif opcion == 3:
    print("LDAP:\n\n")
    ldap_base = u'dc=example,dc=com'
    query = u"(uid=*)"
    con.simple_bind_s(u'cn=admin,dc=example,dc=com', u'admin')
    result = con.search_s(ldap_base, ldap.SCOPE_SUBTREE, query)
    print(result)

    print("MySQL:\n\n")
    cursor.execute('select * from usuarios_ldap;')
    database = cursor.fetchall() 
    print(database)

elif opcion == 4:
    con.simple_bind_s(u'cn=admin,dc=example,dc=com', u'admin')
    reader = csv.DictReader(open("directorio.csv"))
    for raw in reader:
        dn = "uid=" + raw["email"] + ",ou=People,dc=example,dc=com"
        con.delete_s(dn)

    cursor.execute('truncate table usuarios_ldap;')
    database = db.commit()

# Cierre de conexiones
con.unbind_s()
server.quit()
cursor.close()
db.close()
